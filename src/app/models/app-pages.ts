export interface AppPages {
    title: string;
    url: string;
    icon: string;
}

export const APP_PAGES: AppPages[] = [
    {
        title: 'Home',
        url: '/home',
        icon: 'home'
    },
    {
        title: 'Rooms',
        url: '/rooms',
        icon: 'business'
    },
    {
        title: 'Learning Modules',
        url: '/learning-modules',
        icon: 'book'
    },
    {
        title: 'Settings',
        url: '/settings',
        icon: 'settings'
    },
    {
        title: 'Logout',
        url: '/',
        icon: 'log-out'
    }
];
