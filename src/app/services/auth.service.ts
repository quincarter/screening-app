import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import {BehaviorSubject, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {User, UserInterface} from '../models/user.interface';
import 'rxjs-compat/add/operator/switchMap';
import 'rxjs-compat/add/observable/of';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private isAuthorized: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
    public readonly isAuthorized$: Observable<boolean> = this.isAuthorized.asObservable();

    private authenticatedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public readonly authenticated$: Observable<boolean> = this.authenticatedSubject.asObservable();

    user$: Observable<User>;
    private userData: Promise<void>;

    constructor(
        private afs: AngularFirestore,
        private router: Router,
        public angularFireAuth: AngularFireAuth,
    ) {
        //// Get auth data, then get firestore user document || null
        this.user$ = this.angularFireAuth.authState.switchMap(user => {
            if (user) {
                return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
            } else {
                return Observable.of(null);
            }
        });
    }

    private static async setUserData(userRef, user: UserInterface): Promise<void> {
        const data: User = {
            uid: user.uid,
            email: user.email,
            roles: {
                admin: false,
                recruiter: false,
                readonly: true,
            }
        };
        return await userRef.set(data, {merge: true});
    }

    setAuthorized(value) {
        this.isAuthorized.next(value);
    }

    async loginUser(newEmail: string, newPassword: string): Promise<any> {
        return await this.angularFireAuth.auth.signInWithEmailAndPassword(newEmail, newPassword).then((data) => {
            this.updateUserData(data.user);
        });
    }

    async resetPassword(email: string): Promise<void> {
        return await this.angularFireAuth.auth.sendPasswordResetEmail(email);
    }

    async logoutUser(): Promise<void> {
        this.setAuthorized(false);
        return await this.angularFireAuth.auth.signOut();
    }

    async signupUser(newEmail: string, newPassword: string): Promise<any> {
        return await this.angularFireAuth.auth.createUserWithEmailAndPassword(newEmail, newPassword).then((data) => {
            this.updateUserData(data.user);
        });
    }

    googleLogin() {
        const provider = new firebase.auth.GoogleAuthProvider();
        return this.oAuthLogin(provider);
    }

    oAuthLogin(provider) {
        return this.angularFireAuth.auth.signInWithPopup(provider)
            .then((credential) => {
                this.updateUserData(credential.user);
            });
    }

    getAuthUserId(): string {
        return this.angularFireAuth.auth.currentUser.uid;
    }

    public updateUserData(user) {
        // Sets user data to firestore on login
        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
        this.afs.collection('users').doc(user.uid).valueChanges().subscribe((userData: UserInterface) => {
            if (!userData) {
                this.userData = AuthService.setUserData(userRef, user);
            } else {
                this.userData = userRef.set(userData, {merge: true});
            }
        });
    }

    public setAuthenticated(auth: boolean) {
        this.authenticatedSubject.next(auth);
    }

    ///// Role-based Authorization //////

    canRead(user: User): boolean {
        const allowed = ['admin', 'recruiter', 'readonly'];
        return this.checkAuthorization(user, allowed);
    }

    canEdit(user: User): boolean {
        const allowed = ['admin'];
        return this.checkAuthorization(user, allowed);
    }

    canDelete(user: User): boolean {
        const allowed = ['admin'];
        return this.checkAuthorization(user, allowed);
    }


// determines if user has matching role
    checkAuthorization(user: User, allowedRoles: string[]): boolean {
        if (!user) {
            return false;
        }
        for (const role of allowedRoles) {
            if (user.roles[role]) {
                return true;
            }
        }
        return false;
    }
}
