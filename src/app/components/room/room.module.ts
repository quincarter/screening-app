import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RoomListComponent} from './room-list/room-list.component';
import {RoomItemComponent} from './room-item/room-item.component';

@NgModule({
  declarations: [
      RoomListComponent,
      RoomItemComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
      RoomItemComponent,
      RoomListComponent,
  ]
})
export class RoomModule { }
