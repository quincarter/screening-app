import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToolbarModule} from './toolbar/toolbar.module';


@NgModule({
    declarations: [],
    imports: [
        ToolbarModule,
        CommonModule,
    ],
    exports: []
})
export class SharedModule {
}
