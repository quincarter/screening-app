import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardItemComponent} from './dashboard-item/dashboard-item.component';
import {DashboardListComponent} from './dashboard-list/dashboard-list.component';
import {IonicModule} from '@ionic/angular';



@NgModule({
  declarations: [DashboardItemComponent, DashboardListComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
      DashboardListComponent,
      DashboardItemComponent
  ]
})
export class DashboardModule { }
