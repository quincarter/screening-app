import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {NavController} from '@ionic/angular';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {APP_PAGES, AppPages} from '../../models/app-pages';
import {Observable, Subscription} from 'rxjs';
import {User} from '../../models/user.interface';

@Component({
    selector: 'app-base',
    template: '',
})
export class BaseComponent implements OnInit, OnDestroy {
    public isAuthorized: boolean;
    public user: User;
    public rootPage: string;
    public appPages: AppPages[] = APP_PAGES;
    public currentUrl: string;
    public authenticated$: Observable<boolean>;
    public authSubscription: Subscription;

    constructor(
        protected angularFireAuth: AngularFireAuth,
        protected navCtrl: NavController,
        protected authService: AuthService,
        protected router: Router,
    ) {
        this.initApp();
    }

    ngOnInit() {
        this.initApp();
    }

    ngOnDestroy(): void {
        this.authSubscription.unsubscribe();
    }

    public checkUserAuth() {
        const authObserver = this.angularFireAuth.authState.subscribe(user => {
            this.authService.setAuthorized(!!user);
            const url: string = this.router.url;
            if (user) {
                if (url) {
                    if (this.findValidUrl(url)) {
                        this.rootPage = url;
                        this.navCtrl.navigateRoot(this.rootPage);
                        authObserver.unsubscribe();
                    } else {
                        this.rootPage = '/home';
                        this.navCtrl.navigateRoot(this.rootPage);
                        authObserver.unsubscribe();
                    }
                }
            } else {
                if (!this.router.url.startsWith('/room')) {
                    this.rootPage = '/login';
                    this.navCtrl.navigateRoot(this.rootPage);
                    authObserver.unsubscribe();
                } else {
                    this.rootPage = url;
                    this.navCtrl.navigateRoot(this.rootPage);
                    authObserver.unsubscribe();
                }
            }
        });
    }

    public findValidUrl(url: string): any {
        return this.appPages.filter(el => el.url.toLowerCase().indexOf(url.toLowerCase()) !== -1);
    }

    public getAuthSubscription(): void {
        this.authSubscription = this.angularFireAuth.authState.subscribe(data => {
            if (data) {
                this.authService.setAuthenticated(true);
            } else {
                this.authService.setAuthenticated(false);
            }
        });
    }


    public logout() {
        this.authService.logoutUser().then(r => this.checkUserAuth());
    }

    public initApp() {
        this.currentUrl = this.router.url;
        this.authService.user$.subscribe(user => this.user = user);
        this.authService.isAuthorized$.subscribe((data) => {
            this.isAuthorized = data;
        });
        this.getAuthSubscription();
        this.checkUserAuth();
    }
}
