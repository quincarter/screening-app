import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';

import {HomePage} from './home.page';
import {LoggedInGuard} from '../../guards/logged-in.guard';
import {HighlightModule} from 'ngx-highlightjs';
import {BaseModule} from '../../components/base/base.module';
import {DashboardModule} from '../../components/dashboard/dashboard.module';
import {ToolbarModule} from '../../components/shared/toolbar/toolbar.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HighlightModule,
        BaseModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage,
                canActivate: [LoggedInGuard]
            }
        ]),
        DashboardModule,
        ToolbarModule
    ],
    declarations: [HomePage]
})
export class HomePageModule {
}
