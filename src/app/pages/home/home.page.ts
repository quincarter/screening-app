import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {NavController} from '@ionic/angular';
import {AuthService} from '../../services/auth.service';
import {BaseComponent} from '../../components/base/base.component';
import {HighlightResult} from 'ngx-highlightjs';
import {Router} from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage extends BaseComponent implements OnInit, OnDestroy {
    code = `import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';

import {HomePage} from './home.page';
import {LoggedInGuard} from '../guards/logged-in.guard';
import {HighlightModule} from 'ngx-highlightjs';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HighlightModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage,
                canActivate: [LoggedInGuard]
            }
        ])
    ],
    declarations: [HomePage]
})
export class HomePageModule {
}
`;
    private response: HighlightResult;
    pageTitle = 'Home';
    pageColor = 'primary';

    constructor(
        angularFireAuth: AngularFireAuth,
        navCtrl: NavController,
        authService: AuthService,
        router: Router,
    ) {
        super(angularFireAuth, navCtrl, authService, router);
    }

    onHighlight($event: HighlightResult) {
        this.response = {
            language: $event.language,
            r: $event.r,
            second_best: '{...}',
            top: '{...}',
            value: '{...}'
        };
    }
}
