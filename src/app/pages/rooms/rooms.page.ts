import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../../components/base/base.component';
import {AngularFireAuth} from '@angular/fire/auth';
import {NavController} from '@ionic/angular';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-rooms',
    templateUrl: './rooms.page.html',
    styleUrls: ['./rooms.page.scss'],
})
export class RoomsPage extends BaseComponent implements OnInit {
    pageTitle = 'Rooms';
    pageColor = 'tertiary';

    constructor(
        angularFireAuth: AngularFireAuth,
        navCtrl: NavController,
        authService: AuthService,
        router: Router,
    ) {
        super(angularFireAuth, navCtrl, authService, router);
    }

}
