import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {RoomPage} from './room.page';
import {ToolbarModule} from '../../../components/shared/toolbar/toolbar.module';

const routes: Routes = [
    {
        path: ':roomKey',
        component: RoomPage,
    },
    {
        path: '**',
        redirectTo: '/rooms',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ToolbarModule
    ],
    declarations: [RoomPage]
})
export class RoomPageModule {
}
