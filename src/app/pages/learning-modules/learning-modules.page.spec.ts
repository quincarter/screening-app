import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearningModulesPage } from './learning-modules.page';

describe('LearningModulesPage', () => {
  let component: LearningModulesPage;
  let fixture: ComponentFixture<LearningModulesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearningModulesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearningModulesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
