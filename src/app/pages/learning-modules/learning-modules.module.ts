import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LearningModulesPage } from './learning-modules.page';
import {ToolbarModule} from '../../components/shared/toolbar/toolbar.module';

const routes: Routes = [
  {
    path: '',
    component: LearningModulesPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ToolbarModule
    ],
  declarations: [LearningModulesPage]
})
export class LearningModulesPageModule {}
