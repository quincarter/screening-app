import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../../components/base/base.component';
import {AngularFireAuth} from '@angular/fire/auth';
import {NavController} from '@ionic/angular';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-learning-modules',
    templateUrl: './learning-modules.page.html',
    styleUrls: ['./learning-modules.page.scss'],
})
export class LearningModulesPage extends BaseComponent implements OnInit {
    pageTitle = 'Learning Modules';
    pageColor = 'secondary';

    constructor(
        angularFireAuth: AngularFireAuth,
        navCtrl: NavController,
        authService: AuthService,
        router: Router,
    ) {
        super(angularFireAuth, navCtrl, authService, router);
    }
}
