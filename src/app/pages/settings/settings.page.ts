import {Component, OnInit} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {NavController} from '@ionic/angular';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {BaseComponent} from '../../components/base/base.component';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
})
export class SettingsPage extends BaseComponent implements OnInit {
    pageTitle = 'Settings';
    pageColor = 'success';

    constructor(
        angularFireAuth: AngularFireAuth,
        navCtrl: NavController,
        authService: AuthService,
        router: Router,
    ) {
        super(angularFireAuth, navCtrl, authService, router);
    }

    goToResetPassword() {
        this.navCtrl.navigateForward('/reset-password');
    }
}
