import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SettingsPage } from './settings.page';
import {LoggedInGuard} from '../../guards/logged-in.guard';
import {ToolbarModule} from '../../components/shared/toolbar/toolbar.module';

const routes: Routes = [
  {
    path: '',
    component: SettingsPage,
    canActivate: [LoggedInGuard],
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ToolbarModule
    ],
  declarations: [SettingsPage]
})
export class SettingsPageModule {}
