import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {SignUpPage} from './sign-up.page';
import {LoggedOutGuard} from '../../guards/logged-out.guard';
import {ToolbarModule} from '../../components/shared/toolbar/toolbar.module';

const routes: Routes = [
    {
        path: '',
        component: SignUpPage,
        canActivate: [LoggedOutGuard]

    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        ToolbarModule
    ],
    declarations: [SignUpPage]
})
export class SignUpPageModule {
}
