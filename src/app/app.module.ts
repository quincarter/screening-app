import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AuthService} from './services/auth.service';
import typescript from 'highlight.js/lib/languages/typescript';
import css from 'highlight.js/lib/languages/css';
import scss from 'highlight.js/lib/languages/scss';
import less from 'highlight.js/lib/languages/less';
import javascript from 'highlight.js/lib/languages/javascript';
import htmlbars from 'highlight.js/lib/languages/htmlbars';
import java from 'highlight.js/lib/languages/java';
import json from 'highlight.js/lib/languages/json';
import dockerfile from 'highlight.js/lib/languages/dockerfile';
import php from 'highlight.js/lib/languages/php';
import xml from 'highlight.js/lib/languages/xml';
import python from 'highlight.js/lib/languages/python';
import {HIGHLIGHT_OPTIONS} from 'ngx-highlightjs';
import {BaseModule} from './components/base/base.module';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireStorageModule} from '@angular/fire/storage';

export function hljsLanguages() {
    return [
        {name: 'typescript', func: typescript},
        {name: 'css', func: css},
        {name: 'scss', func: scss},
        {name: 'less', func: less},
        {name: 'php', func: php},
        {name: 'dockerfile', func: dockerfile},
        {name: 'python', func: python},
        {name: 'java', func: java},
        {name: 'json', func: json},
        {name: 'javascript', func: javascript},
        {name: 'html', func: htmlbars},
        {name: 'xml', func: xml}
    ];
}

@NgModule({
    declarations: [AppComponent],
    entryComponents: [AppComponent],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireStorageModule,
        AngularFireAuthModule,
        AppRoutingModule,
        BaseModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        AuthService,
        {
            provide: HIGHLIGHT_OPTIONS,
            useValue: {
                languages: hljsLanguages,
                tabReplace: true,
                config: {
                    initHighlightingOnLoad: true,
                }
            }
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
