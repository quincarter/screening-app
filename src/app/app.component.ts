import {Component, OnDestroy, OnInit} from '@angular/core';

import {NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Subscription} from 'rxjs';
import {BaseComponent} from './components/base/base.component';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthService} from './services/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent extends BaseComponent implements OnInit, OnDestroy {
    public authSubscription: Subscription;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        angularFireAuth: AngularFireAuth,
        navCtrl: NavController,
        authService: AuthService,
        router: Router,
    ) {
        super(angularFireAuth, navCtrl, authService, router);
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
}
