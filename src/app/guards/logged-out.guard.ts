import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedOutGuard implements CanActivate {
  private isLoggedOut: boolean;

  constructor(
      private authService: AuthService
  ) {
  }

  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkUserAuth().then(r => this.isLoggedOut);
  }

  private async checkUserAuth() {
    await this.authService.angularFireAuth.authState.subscribe(user => {
      if (!user) {
        this.isLoggedOut = true;
      } else {
        this.isLoggedOut = false;
      }
    });
  }
}
