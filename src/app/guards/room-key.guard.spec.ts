import { TestBed, async, inject } from '@angular/core/testing';

import { RoomKeyGuard } from './room-key.guard';

describe('RoomKeyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RoomKeyGuard]
    });
  });

  it('should ...', inject([RoomKeyGuard], (guard: RoomKeyGuard) => {
    expect(guard).toBeTruthy();
  }));
});
