import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class LoggedInGuard implements CanActivate {
    private isLoggedIn: boolean;

    constructor(
        private authService: AuthService
    ) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.checkUserAuth().then(r => this.isLoggedIn);
    }

    private async checkUserAuth() {
        await this.authService.angularFireAuth.authState.subscribe(user => {
            this.isLoggedIn = !!user;
        });
    }
}
